cmake_minimum_required(VERSION 3.10)

project(BeerBobProtocol)


add_subdirectory(flatbuffers)



add_library(BeerBobProtocol)


target_link_libraries(BeerBobProtocol
        PRIVATE
        flatbuffers)


